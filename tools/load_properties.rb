#!/usr/bin/ruby
require 'json'
require 'net/http'

uri = URI.parse('http://localhost:8080/properties')
header = {'Content-Type' => 'application/json'}
http = Net::HTTP.new(uri.host, uri.port)
request = Net::HTTP::Post.new(uri.request_uri, header)

json_filename = File.expand_path(File.dirname(__FILE__))+"/../docs/code-challenge/properties.json"
json_data = JSON.parse(File.read(json_filename))

properties_amount = json_data['totalProperties']
json_data['properties'].each do |property|
    puts "Processing property with id " + property['id'].to_s
    request.body = property.to_json
    response = http.request(request)
    if response.code != "200" then
        puts "Aborting script: "+response.body
        exit
    end    
end
