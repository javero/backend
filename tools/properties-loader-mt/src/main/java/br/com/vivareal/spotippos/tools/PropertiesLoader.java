package br.com.vivareal.spotippos.tools;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.FileReader;

public class PropertiesLoader {

    private static final Logger LOGGER = LoggerFactory.getLogger(PropertiesLoader.class);
    private static final Integer SIMULTANEOUS_THREADS = 20;

    public static void main(String... args) {
        if (args.length != 1) {
            System.err.println("The properties filename must be provided as first argument.");
            System.exit(-1);
        }

        JSONParser parser = new JSONParser();
        try {
            Object obj = parser.parse(new FileReader(args[0]));
            JSONObject jsonObject = (JSONObject) obj;
            JSONArray properties = (JSONArray) jsonObject.get("properties");

            Worker[] workers = buildWorkers();
            for (int i = 0; i < properties.size(); i++) {
                JSONObject property = (JSONObject) properties.get(i);
                workers[i % SIMULTANEOUS_THREADS].add(property.toJSONString());
            }
            startWorkers(workers);

        } catch (Exception e) {
            System.err.println("Error during JSON parsing: " + e.getMessage());
        }
    }

    private static void startWorkers(final Worker[] workers) {
        for (int i = 0; i < SIMULTANEOUS_THREADS; i++) {
            new Thread(workers[i]).start();
        }
    }

    private static Worker[] buildWorkers() {
        Worker[] result = new Worker[SIMULTANEOUS_THREADS];
        for (int i = 0; i < SIMULTANEOUS_THREADS; i++) {
            result[i] = new Worker(i + 1);
        }
        return result;
    }
}
