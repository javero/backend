package br.com.vivareal.spotippos.tools;

import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.HttpClientBuilder;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;

public class Worker implements Runnable {
    private final Integer id;
    private Integer totalRequests = 0;
    List<StringEntity> stringEntitiesList = new ArrayList<StringEntity>();

    public Worker(Integer id) {
        this.id = id;
    }

    public void run() {
        while (!stringEntitiesList.isEmpty()) {
            HttpClient httpClient = HttpClientBuilder.create().build();
            HttpPost request = new HttpPost("http://localhost:8080/properties");
            request.addHeader("content-type", "application/json");
            try {
                request.setEntity(stringEntitiesList.remove(0));
                totalRequests++;
                System.out.println("Worker "+id+" - total requests = "+totalRequests);
                httpClient.execute(request);
            } catch (Exception e) {
                System.err.println("Worker "+id+" - "+e);
            }
        }
    }


    public void add(String propertyAttributes) throws UnsupportedEncodingException {
        stringEntitiesList.add(new StringEntity(propertyAttributes,
                "application/json", "UTF-8"));
    }
}
