package br.com.vivareal.spotippos.services;

import br.com.vivareal.spotippos.domain.Property;
import br.com.vivareal.spotippos.domain.Province;
import br.com.vivareal.spotippos.domain.exceptions.SequenceException;
import br.com.vivareal.spotippos.rs.PropertiesEndpoint;
import org.glassfish.jersey.message.internal.OutboundJaxrsResponse;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpStatus;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.HashMap;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.when;

@RunWith(SpringRunner.class)
@SpringBootTest
public class PropertiesCreationTest extends AbstractPropertiesServicesTest {

    private static final String ERROR_DESCRIPTION_FIELD = "description";
    private static final String ERROR_LATITUDE_RANGE = "Latitude must be between 0 and 1400";
    private static final String ERROR_LONGITUDE_RANGE = "Longitude must be between 0 and 1000";
    private static final String ERROR_BEDS_RANGE = "Beds must be between 1 and 5";
    private static final String ERROR_BATHS_RANGE = "Baths must be between 1 and 4";
    private static final String ERROR_SQUARE_METERS_RANGE = "Square meters must be between 20 and 240";
    private static final String INTERNAL_ERROR = "Internal error";

    @Test
    public void shouldPersistAllFieldsCorrectly() {
        Property property = buildAnyValidPropertyInRandomCoordinate();

        OutboundJaxrsResponse response = (OutboundJaxrsResponse) propertiesEndpoint.addProperty(property);
        assertThat(response.getStatusInfo().getStatusCode()).isEqualTo(HttpStatus.OK.value());
        Property createdProperty = (Property) response.getContext().getEntity();
        assertThat(createdProperty.getBaths()).isEqualTo(createdProperty.getBaths());
        assertThat(createdProperty.getBeds()).isEqualTo(createdProperty.getBeds());
        assertThat(createdProperty.getDescription()).isEqualTo(createdProperty.getDescription());
        assertThat(createdProperty.getLat()).isEqualTo(createdProperty.getLat());
        assertThat(createdProperty.getLong()).isEqualTo(createdProperty.getLong());
        assertThat(createdProperty.getPrice()).isEqualTo(createdProperty.getPrice());
        assertThat(createdProperty.getSquareMeters()).isEqualTo(createdProperty.getSquareMeters());
    }

    @Test
    public void shouldNotAcceptNegativeLatitude() {
        Property property = buildAnyValidPropertyInRandomCoordinate();
        property.setLat(-1);

        OutboundJaxrsResponse response = (OutboundJaxrsResponse) propertiesEndpoint.addProperty(property);
        assertThat(response.getStatusInfo().getStatusCode()).isEqualTo(HttpStatus.BAD_REQUEST.value());
        assertThat(response.getContext().getEntity()).isInstanceOf(HashMap.class);
        HashMap<String, String> responseContents = (HashMap<String, String>) response.getContext().getEntity();
        assertThat(responseContents.get(ERROR_DESCRIPTION_FIELD)).isEqualTo(ERROR_LATITUDE_RANGE);
    }

    @Test
    public void shouldAcceptZeroLatitude() {
        Property property = buildAnyValidPropertyInRandomCoordinate();
        property.setLat(0);

        OutboundJaxrsResponse response = (OutboundJaxrsResponse) propertiesEndpoint.addProperty(property);
        assertThat(response.getStatusInfo().getStatusCode()).isEqualTo(HttpStatus.OK.value());
        Property createdProperty = (Property) response.getContext().getEntity();
        assertThat(createdProperty.getLat()).isEqualTo(createdProperty.getLat());
    }

    @Test
    public void shouldNotAcceptLatitudeAboveLimit() {
        Property property = buildAnyValidPropertyInRandomCoordinate();
        property.setLat(1401);

        OutboundJaxrsResponse response = (OutboundJaxrsResponse) propertiesEndpoint.addProperty(property);
        assertThat(response.getStatusInfo().getStatusCode()).isEqualTo(HttpStatus.BAD_REQUEST.value());
        assertThat(response.getContext().getEntity()).isInstanceOf(HashMap.class);
        HashMap<String, String> responseContents = (HashMap<String, String>) response.getContext().getEntity();
        assertThat(responseContents.get(ERROR_DESCRIPTION_FIELD)).isEqualTo(ERROR_LATITUDE_RANGE);
    }

    @Test
    public void shouldNotAcceptNegativeLongitude() {
        Property property = buildAnyValidPropertyInRandomCoordinate();
        property.setLong(-1);

        OutboundJaxrsResponse response = (OutboundJaxrsResponse) propertiesEndpoint.addProperty(property);
        assertThat(response.getStatusInfo().getStatusCode()).isEqualTo(HttpStatus.BAD_REQUEST.value());
        assertThat(response.getContext().getEntity()).isInstanceOf(HashMap.class);
        HashMap<String, String> responseContents = (HashMap<String, String>) response.getContext().getEntity();
        assertThat(responseContents.get(ERROR_DESCRIPTION_FIELD)).isEqualTo(ERROR_LONGITUDE_RANGE);
    }

    @Test
    public void shouldAcceptZeroLongitude() {
        Property property = buildAnyValidPropertyInRandomCoordinate();
        property.setLong(0);

        OutboundJaxrsResponse response = (OutboundJaxrsResponse) propertiesEndpoint.addProperty(property);
        assertThat(response.getStatusInfo().getStatusCode()).isEqualTo(HttpStatus.OK.value());
        Property createdProperty = (Property) response.getContext().getEntity();
        assertThat(createdProperty.getLong()).isEqualTo(0);
    }

    @Test
    public void shouldNotAcceptLongitudeAboveLimit() {
        Property property = buildAnyValidPropertyInRandomCoordinate();
        property.setLong(1001);

        OutboundJaxrsResponse response = (OutboundJaxrsResponse) propertiesEndpoint.addProperty(property);
        assertThat(response.getStatusInfo().getStatusCode()).isEqualTo(HttpStatus.BAD_REQUEST.value());
        assertThat(response.getContext().getEntity()).isInstanceOf(HashMap.class);
        HashMap<String, String> responseContents = (HashMap<String, String>) response.getContext().getEntity();
        assertThat(responseContents.get(ERROR_DESCRIPTION_FIELD)).isEqualTo(ERROR_LONGITUDE_RANGE);
    }

    @Test
    public void shouldNotAcceptNegativeBeds() {
        Property property = buildAnyValidPropertyInRandomCoordinate();
        property.setBeds(-1);

        OutboundJaxrsResponse response = (OutboundJaxrsResponse) propertiesEndpoint.addProperty(property);
        assertThat(response.getStatusInfo().getStatusCode()).isEqualTo(HttpStatus.BAD_REQUEST.value());
        assertThat(response.getContext().getEntity()).isInstanceOf(HashMap.class);
        HashMap<String, String> responseContents = (HashMap<String, String>) response.getContext().getEntity();
        assertThat(responseContents.get(ERROR_DESCRIPTION_FIELD)).isEqualTo(ERROR_BEDS_RANGE);
    }

    @Test
    public void shouldNotAcceptZeroBeds() {
        Property property = buildAnyValidPropertyInRandomCoordinate();
        property.setBeds(0);

        OutboundJaxrsResponse response = (OutboundJaxrsResponse) propertiesEndpoint.addProperty(property);
        assertThat(response.getStatusInfo().getStatusCode()).isEqualTo(HttpStatus.BAD_REQUEST.value());
        assertThat(response.getContext().getEntity()).isInstanceOf(HashMap.class);
        HashMap<String, String> responseContents = (HashMap<String, String>) response.getContext().getEntity();
        assertThat(responseContents.get(ERROR_DESCRIPTION_FIELD)).isEqualTo(ERROR_BEDS_RANGE);
    }

    @Test
    public void shouldNotAcceptBedsAboveLimit() {
        Property property = buildAnyValidPropertyInRandomCoordinate();
        property.setBeds(6);

        OutboundJaxrsResponse response = (OutboundJaxrsResponse) propertiesEndpoint.addProperty(property);
        assertThat(response.getStatusInfo().getStatusCode()).isEqualTo(HttpStatus.BAD_REQUEST.value());
        assertThat(response.getContext().getEntity()).isInstanceOf(HashMap.class);
        HashMap<String, String> responseContents = (HashMap<String, String>) response.getContext().getEntity();
        assertThat(responseContents.get(ERROR_DESCRIPTION_FIELD)).isEqualTo(ERROR_BEDS_RANGE);
    }

    @Test
    public void shouldNotAcceptNegativeBaths() {
        Property property = buildAnyValidPropertyInRandomCoordinate();
        property.setBaths(-1);

        OutboundJaxrsResponse response = (OutboundJaxrsResponse) propertiesEndpoint.addProperty(property);
        assertThat(response.getStatusInfo().getStatusCode()).isEqualTo(HttpStatus.BAD_REQUEST.value());
        assertThat(response.getContext().getEntity()).isInstanceOf(HashMap.class);
        HashMap<String, String> responseContents = (HashMap<String, String>) response.getContext().getEntity();
        assertThat(responseContents.get(ERROR_DESCRIPTION_FIELD)).isEqualTo(ERROR_BATHS_RANGE);
    }

    @Test
    public void shouldNotAcceptZeroBaths() {
        Property property = buildAnyValidPropertyInRandomCoordinate();
        property.setBaths(0);

        OutboundJaxrsResponse response = (OutboundJaxrsResponse) propertiesEndpoint.addProperty(property);
        assertThat(response.getStatusInfo().getStatusCode()).isEqualTo(HttpStatus.BAD_REQUEST.value());
        assertThat(response.getContext().getEntity()).isInstanceOf(HashMap.class);
        HashMap<String, String> responseContents = (HashMap<String, String>) response.getContext().getEntity();
        assertThat(responseContents.get(ERROR_DESCRIPTION_FIELD)).isEqualTo(ERROR_BATHS_RANGE);
    }

    @Test
    public void shouldNotAcceptBathsAboveLimit() {
        Property property = buildAnyValidPropertyInRandomCoordinate();
        property.setBaths(5);

        OutboundJaxrsResponse response = (OutboundJaxrsResponse) propertiesEndpoint.addProperty(property);
        assertThat(response.getStatusInfo().getStatusCode()).isEqualTo(HttpStatus.BAD_REQUEST.value());
        assertThat(response.getContext().getEntity()).isInstanceOf(HashMap.class);
        HashMap<String, String> responseContents = (HashMap<String, String>) response.getContext().getEntity();
        assertThat(responseContents.get(ERROR_DESCRIPTION_FIELD)).isEqualTo(ERROR_BATHS_RANGE);
    }

    @Test
    public void shouldNotAcceptNegativeSquareMeters() {
        Property property = buildAnyValidPropertyInRandomCoordinate();
        property.setSquareMeters(-1);

        OutboundJaxrsResponse response = (OutboundJaxrsResponse) propertiesEndpoint.addProperty(property);
        assertThat(response.getStatusInfo().getStatusCode()).isEqualTo(HttpStatus.BAD_REQUEST.value());
        assertThat(response.getContext().getEntity()).isInstanceOf(HashMap.class);
        HashMap<String, String> responseContents = (HashMap<String, String>) response.getContext().getEntity();
        assertThat(responseContents.get(ERROR_DESCRIPTION_FIELD)).isEqualTo(ERROR_SQUARE_METERS_RANGE);
    }

    @Test
    public void shouldNotAcceptZeroSquareMeters() {
        Property property = buildAnyValidPropertyInRandomCoordinate();
        property.setSquareMeters(0);

        OutboundJaxrsResponse response = (OutboundJaxrsResponse) propertiesEndpoint.addProperty(property);
        assertThat(response.getStatusInfo().getStatusCode()).isEqualTo(HttpStatus.BAD_REQUEST.value());
        assertThat(response.getContext().getEntity()).isInstanceOf(HashMap.class);
        HashMap<String, String> responseContents = (HashMap<String, String>) response.getContext().getEntity();
        assertThat(responseContents.get(ERROR_DESCRIPTION_FIELD)).isEqualTo(ERROR_SQUARE_METERS_RANGE);
    }

    @Test
    public void shouldNotAcceptSquareMetersBelowLimit() {
        Property property = buildAnyValidPropertyInRandomCoordinate();
        property.setSquareMeters(19);

        OutboundJaxrsResponse response = (OutboundJaxrsResponse) propertiesEndpoint.addProperty(property);
        assertThat(response.getStatusInfo().getStatusCode()).isEqualTo(HttpStatus.BAD_REQUEST.value());
        assertThat(response.getContext().getEntity()).isInstanceOf(HashMap.class);
        HashMap<String, String> responseContents = (HashMap<String, String>) response.getContext().getEntity();
        assertThat(responseContents.get(ERROR_DESCRIPTION_FIELD)).isEqualTo(ERROR_SQUARE_METERS_RANGE);
    }

    @Test
    public void shouldNotAcceptSquareMetersAboveLimit() {
        Property property = buildAnyValidPropertyInRandomCoordinate();
        property.setSquareMeters(241);

        OutboundJaxrsResponse response = (OutboundJaxrsResponse) propertiesEndpoint.addProperty(property);
        assertThat(response.getStatusInfo().getStatusCode()).isEqualTo(HttpStatus.BAD_REQUEST.value());
        assertThat(response.getContext().getEntity()).isInstanceOf(HashMap.class);
        HashMap<String, String> responseContents = (HashMap<String, String>) response.getContext().getEntity();
        assertThat(responseContents.get(ERROR_DESCRIPTION_FIELD)).isEqualTo(ERROR_SQUARE_METERS_RANGE);
    }

    @Test
    public void shouldCreateAtRightProvince() {
        for (Province province: provinceRepository.findAll()) {
            Property property = buildValidPropertyInMiddleOfProvince(province);
            OutboundJaxrsResponse response = (OutboundJaxrsResponse) propertiesEndpoint.addProperty(property);
            assertThat(response.getStatusInfo().getStatusCode()).isEqualTo(HttpStatus.OK.value());
            assertThat(response.getContext().getEntity()).isInstanceOf(Property.class);
            Property createdProperty = (Property) response.getContext().getEntity();
            assertThat(createdProperty.getProvinces()).contains(province.getName());
        }
    }

    @InjectMocks
    private PropertiesEndpoint mockedPropertiesEndpoint;
    @Mock
    private PropertiesService mockedPropertiesService;

    @Test
    public void shouldHandleSequenceException() throws SequenceException {
        when(mockedPropertiesEndpoint.addProperty(any())).thenCallRealMethod();
        doThrow(new SequenceException("")).when(mockedPropertiesService).insert(any());


        Property property = buildAnyValidPropertyInRandomCoordinate();
        OutboundJaxrsResponse response = (OutboundJaxrsResponse) mockedPropertiesEndpoint.addProperty(property);
        assertThat(response.getStatusInfo().getStatusCode()).isEqualTo(HttpStatus.BAD_REQUEST.value());
        HashMap<String, String> responseContents = (HashMap<String, String>) response.getContext().getEntity();
        assertThat(responseContents.get(ERROR_DESCRIPTION_FIELD)).isEqualTo(INTERNAL_ERROR);
    }
}
