package br.com.vivareal.spotippos.services;

import br.com.vivareal.spotippos.domain.Property;
import br.com.vivareal.spotippos.domain.Province;
import br.com.vivareal.spotippos.domain.repositories.ProvinceRepository;
import br.com.vivareal.spotippos.rs.PropertiesEndpoint;
import org.junit.Before;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.mongodb.MongoDbFactory;

import javax.inject.Inject;

@Configuration
public abstract class AbstractPropertiesServicesTest {
    @Value("${spring.data.mongodb.database}")
    protected String database;

    @Autowired
    protected MongoDbFactory mongo;

    @Inject
    protected ProvinceRepository provinceRepository;

    @Inject
    protected PropertiesEndpoint propertiesEndpoint;

    @Before
    public void cleanUp() {
        mongo.getDb(database).getCollection("property").drop();
    }

    protected Property buildValidPropertyIn(Integer latitude, Integer longitude) {
        return Property.build(latitude, longitude,
                "Valid Title", 1000000L, "Valid description", 2, 2, 100);
    }

    protected Property buildAnyValidPropertyInRandomCoordinate() {
        Integer randomLatitude = (int)(Math.random() * 1400);
        Integer randomLongitude = (int)(Math.random() * 1000);
        return buildValidPropertyIn(randomLatitude, randomLongitude);
    }

    protected Property buildValidPropertyInMiddleOfProvince(Province province) {
        Integer latitude = province.getBLatitude()+(province.getALatitude()-province.getBLatitude())/2;
        Integer longitude = province.getALongitude()+(province.getBLongitude()-province.getALongitude())/2;

        return Property.build(latitude, longitude,
                "Property at [lat,long] = ["+latitude+","+longitude+"]",
                1000000L,
                "Valid description",
                2, 2, 100);
    }

}
