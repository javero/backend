package br.com.vivareal.spotippos.services;

import br.com.vivareal.spotippos.domain.Property;
import br.com.vivareal.spotippos.domain.Province;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpStatus;
import org.springframework.test.context.junit4.SpringRunner;

import javax.ws.rs.core.Response;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

@RunWith(SpringRunner.class)
@SpringBootTest
public class PropertiesFindTest extends AbstractPropertiesServicesTest {

    @Test
    public void findByUnknownIdShouldReturnNotFound() {
        Response response = propertiesEndpoint.getProperty(1L);
        assertThat(response.getStatusInfo().getStatusCode()).isEqualTo(HttpStatus.NOT_FOUND.value());
    }

    @Test
    public void findByIdShouldReturnOneProperty() {
        String title = "testShouldReturnOneProperty - "+ Calendar.getInstance().getTimeInMillis();
        Property property = buildAnyValidPropertyInRandomCoordinate();
        property.setTitle(title);
        propertiesEndpoint.addProperty(property);

        Response response = propertiesEndpoint.getProperty(property.getId());
        assertThat(response.getStatusInfo().getStatusCode()).isEqualTo(HttpStatus.OK.value());
        assertThat(((Property) response.getEntity()).getTitle()).isEqualTo(title);
    }

    @Test
    public void findInNotPopulatedRegionShouldReturnZeroProperties() {
        List<Province> provinceList = provinceRepository.findAll();
        Province province1 = provinceList.get(0);
        Property property1 = buildValidPropertyInMiddleOfProvince(province1);
        propertiesEndpoint.addProperty(property1);
        Property property2 = buildValidPropertyInMiddleOfProvince(province1);
        propertiesEndpoint.addProperty(property2);

        provinceList.remove(0);
        for (Province province: provinceList) {
            Response response = propertiesEndpoint.getPropertiesInRegion(
                    province.getALatitude(), province.getALongitude(),
                    province.getBLatitude(), province.getBLatitude());
            assertThat(response.getStatusInfo().getStatusCode()).isEqualTo(HttpStatus.OK.value());
            HashMap<String, Object> responseBody = (HashMap<String, Object>) response.getEntity();
            assertThat(responseBody.get("foundProperties")).isEqualTo(0);
            assertThat((List) responseBody.get("properties")).isEmpty();
        }
    }

    @Test
    public void findPropertyInRegionsIntersectionShouldReturnMultipleProvinceNames() {
        Property property = buildValidPropertyIn(500, 750);
        propertiesEndpoint.addProperty(property);

        Response response = propertiesEndpoint.getPropertiesInRegion(400, 1000, 600, 500);
        assertThat(response.getStatusInfo().getStatusCode()).isEqualTo(HttpStatus.OK.value());
        HashMap<String, Object> responseBody = (HashMap<String, Object>) response.getEntity();
        assertThat(responseBody.get("foundProperties")).isEqualTo(1);
        assertThat(((List) responseBody.get("properties")).size()).isEqualTo(1);
        Property foundProperty = (Property) ((List) responseBody.get("properties")).get(0);
        assertThat(foundProperty.getProvinces().size()).isEqualTo(2);
    }

    @Test
    public void findPropertyInInvalidRegionsShouldReturnZeroProperties() {
        Property property = buildAnyValidPropertyInRandomCoordinate();
        propertiesEndpoint.addProperty(property);

        Response response = propertiesEndpoint.getPropertiesInRegion(-1, 0, 0, -1);
        assertThat(response.getStatusInfo().getStatusCode()).isEqualTo(HttpStatus.OK.value());
        HashMap<String, Object> responseBody = (HashMap<String, Object>) response.getEntity();
        assertThat(responseBody.get("foundProperties")).isEqualTo(0);
        assertThat(((List) responseBody.get("properties")).size()).isEqualTo(0);
    }

    @Test
    public void findPropertiesInAllMapShouldReturnAllProperties() {
        final int amountProperties = 100;
        for (int i = 0; i < amountProperties; i++) {
            propertiesEndpoint.addProperty(buildAnyValidPropertyInRandomCoordinate());
        }

        Response response = propertiesEndpoint.getPropertiesInRegion(0, 1000, 1400, 0);
        assertThat(response.getStatusInfo().getStatusCode()).isEqualTo(HttpStatus.OK.value());
        HashMap<String, Object> responseBody = (HashMap<String, Object>) response.getEntity();
        assertThat(responseBody.get("foundProperties")).isEqualTo(amountProperties);
        assertThat(((List) responseBody.get("properties")).size()).isEqualTo(amountProperties);
    }

}
