package br.com.vivareal.spotippos.domain;

import org.hibernate.validator.constraints.NotEmpty;
import org.hibernate.validator.constraints.Range;
import org.springframework.data.mongodb.core.mapping.Document;

import javax.validation.constraints.NotNull;
import java.util.ArrayList;
import java.util.List;

@Document
public class Property {
    private Long id;

    @NotEmpty
    private String title;

    @NotNull
    private Long price;

    @NotEmpty
    private String description;

    @NotNull
    @Range(min = 0, max = 1400, message = "{validation.property.latitude.range}")
    private Integer latitude;

    @NotNull
    @Range(min = 0, max = 1000, message = "{validation.property.longitude.range}")
    private Integer longitude;

    @NotNull
    @Range(min = 1, max = 5, message = "{validation.property.beds.range}")
    private Integer beds;

    @NotNull
    @Range(min = 1, max = 4, message = "{validation.property.baths.range}")
    private Integer baths;

    @NotNull
    @Range(min = 20, max = 240, message = "{validation.property.square_meters.range}")
    private Integer squareMeters;

    private List<String> provinces = new ArrayList<>();

    public Property() {
    }

    private Property(String title, Long price, String description,
                     Integer latitude, Integer longitude, Integer beds, Integer baths,
                     Integer squareMeters) {
        setTitle(title);
        setPrice(price);
        setDescription(description);
        setLat(latitude);
        setLong(longitude);
        setBeds(beds);
        setBaths(baths);
        setSquareMeters(squareMeters);
    }

    public static Property build(Integer latitude, Integer longitude,
                                 String title, Long price, String description,
                                 Integer beds, Integer baths, Integer squareMeters) {
        return new Property(title, price, description, latitude, longitude,
                beds, baths, squareMeters);
    }

    public Long getId() {
        return id;
    }

    public String getTitle() {
        return title;
    }

    public Long getPrice() {
        return price;
    }

    public String getDescription() {
        return description;
    }

    public Integer getLat() {
        return latitude;
    }

    public Integer getLong() {
        return longitude;
    }

    public Integer getBeds() {
        return beds;
    }

    public Integer getBaths() {
        return baths;
    }

    public Integer getSquareMeters() {
        return squareMeters;
    }

    public List<String> getProvinces() {
        return provinces;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public void setPrice(Long price) {
        this.price = price;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public void setLat(Integer latitude) {
        this.latitude = latitude;
    }

    public void setLong(Integer longitude) {
        this.longitude = longitude;
    }

    public void setBeds(Integer beds) {
        this.beds = beds;
    }

    public void setBaths(Integer baths) {
        this.baths = baths;
    }

    public void setSquareMeters(Integer squareMeters) {
        this.squareMeters = squareMeters;
    }

//    public void setProvinces(List<String> provinces) {
//        this.provinces = provinces;
//    }

    public void addProvince(String provinceName) {
        provinces.add(provinceName);
    }
}
