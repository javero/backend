package br.com.vivareal.spotippos;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;


@SpringBootApplication
public class ServerRunner {

    public static void main(String[] args) throws Exception {
        SpringApplication.run(ServerRunner.class, args);
    }

}
