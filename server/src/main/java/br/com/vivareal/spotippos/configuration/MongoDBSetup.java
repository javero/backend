package br.com.vivareal.spotippos.configuration;

import com.mongodb.BasicDBObject;
import com.mongodb.DB;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.ApplicationListener;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.mongodb.MongoDbFactory;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.Map;

@Component
@Configuration
public class MongoDBSetup implements ApplicationListener<ApplicationReadyEvent> {

    @Value("${spring.data.mongodb.database}")
    private String database;

    private final MongoDbFactory mongo;
    @Autowired
    public MongoDBSetup(final MongoDbFactory mongo) {
        this.mongo = mongo;
    }

    private static final Logger LOGGER = LoggerFactory.getLogger(MongoDBSetup.class);

    @Override
    public final void onApplicationEvent(final ApplicationReadyEvent event) {
        DB db = mongo.getDb(database);

        LOGGER.info("Creating index for field <latitude> on <property> Mongo DB collection");
        db.getCollection("property").createIndex(new BasicDBObject("latitude",1));

        LOGGER.info("Creating index for field <longitude> on <property> Mongo DB collection");
        db.getCollection("property").createIndex(new BasicDBObject("longitude",1));

        LOGGER.info("Creating sequential identifier handler for collection Property");
        Map<String, Object> map = new HashMap<>();
        map.put("_id", "Property");
        map.put("seq", new Long(0));
        db.getCollection("sequence").insert(new BasicDBObject(map));
    }

}
