package br.com.vivareal.spotippos.domain;


import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document
public class Sequence {
    @Id
    private String id;

    private Long seq;


    public Long getSeq() {
        return seq;
    }
}
