package br.com.vivareal.spotippos.domain.exceptions;

public class SequenceException extends Throwable {

    public SequenceException(String s) {
        super(s);
    }

}
