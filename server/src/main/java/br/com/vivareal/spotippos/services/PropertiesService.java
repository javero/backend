package br.com.vivareal.spotippos.services;

import br.com.vivareal.spotippos.domain.Property;
import br.com.vivareal.spotippos.domain.Province;
import br.com.vivareal.spotippos.domain.exceptions.SequenceException;
import br.com.vivareal.spotippos.domain.repositories.PropertyRepository;
import br.com.vivareal.spotippos.domain.repositories.ProvinceRepository;
import br.com.vivareal.spotippos.domain.repositories.SequenceRepository;
import org.springframework.stereotype.Service;

import javax.inject.Inject;
import java.util.List;

@Service
public class PropertiesService {
    @Inject
    private PropertyRepository propertyRepository;
    @Inject
    private ProvinceRepository provinceRepository;
    @Inject
    private SequenceRepository sequenceRepository;

    public final Property find(Long id) {
        return propertyRepository.findOne(id);
    }

    public void insert(final Property property) throws SequenceException {
        for (Province province: provinceRepository.findAll()) {
            if (province.contains(property)) {
                property.addProvince(province.getName());
            }
        }

        property.setId(sequenceRepository.getNextSequence(property.getClass().getSimpleName()));
        propertyRepository.insert(property);
    }


    public final List<Property> findAllInRegion(final Integer ax, final Integer ay,
                                                final Integer bx, final Integer by) {
        return propertyRepository.findAllInRegion(ax, ay, bx, by);
    }

}
