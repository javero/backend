package br.com.vivareal.spotippos.rs;

import br.com.vivareal.spotippos.domain.Property;
import br.com.vivareal.spotippos.domain.exceptions.SequenceException;
import br.com.vivareal.spotippos.services.PropertiesService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.RequestBody;

import javax.inject.Inject;
import javax.validation.ConstraintViolationException;
import javax.ws.rs.*;
import javax.ws.rs.core.Response;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Component
@Path("properties")
public class PropertiesEndpoint {

    private final Logger logger = LoggerFactory.getLogger(PropertiesEndpoint.class);

    @Inject
    private PropertiesService propertiesService;

    @Produces({"application/json"})
    @Path("{id}")
    @GET
    public final Response getProperty(@PathParam("id") final Long id) {
        Property property = propertiesService.find(id);
        if (property != null) {
            return Response.ok(property).build();
        }
        return Response.status(Response.Status.NOT_FOUND).entity("").build();
    }

    @Produces({"application/json"})
    @Consumes({"application/json"})
    @POST
    public Response addProperty(@RequestBody final Property property) {
        try {
            propertiesService.insert(property);
            return Response.ok(property).build();
        } catch (SequenceException se) {
            logger.error("Sequence for ID could not be generated", se);
            Map<String, String> errors = new HashMap<>();
            errors.put("description", "Internal error");
            return Response.status(Response.Status.BAD_REQUEST).entity(errors).build();
        } catch (ConstraintViolationException cve) {
            logger.error("Constraint violated", cve);
            Map<String, String> errors = new HashMap<>();
            errors.put("description", userFriendlyMessageFrom(cve));
            return Response.status(Response.Status.BAD_REQUEST).entity(errors).build();
        }
    }

    private String userFriendlyMessageFrom(final ConstraintViolationException e) {
        List<String> messages = new ArrayList<>();
        e.getConstraintViolations().stream().forEach((violation) -> {
            messages.add(violation.getMessage());
        });
        return String.join(",", messages);
    }

    @Produces({"application/json"})
    @GET
    public final Response getPropertiesInRegion(@QueryParam("ax") final Integer ax,
                                  @QueryParam("ay") final Integer ay,
                                  @QueryParam("bx") final Integer bx,
                                  @QueryParam("by") final Integer by) {
        Map<String, Object> result = new HashMap<>();
        List<Property> properties = propertiesService.findAllInRegion(ax, ay, bx, by);
        result.put("foundProperties", properties.size());
        result.put("properties", properties);

        return Response.ok(result).build();
    }

}
