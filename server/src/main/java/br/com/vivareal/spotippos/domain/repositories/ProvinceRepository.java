package br.com.vivareal.spotippos.domain.repositories;

import br.com.vivareal.spotippos.domain.Province;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface ProvinceRepository extends CrudRepository<Province, Long> {

    void insert(Province province);

    Province findByName(String name);

    List<Province> findAll();
}
