package br.com.vivareal.spotippos.configuration;

import br.com.vivareal.spotippos.rs.PropertiesEndpoint;
import org.glassfish.jersey.server.ResourceConfig;
import org.glassfish.jersey.server.internal.process.Endpoint;
import org.springframework.stereotype.Component;

@Component
public class JerseyConfig extends ResourceConfig {

    public JerseyConfig() {
        register(Endpoint.class);
        register(PropertiesEndpoint.class);
    }

}
