package br.com.vivareal.spotippos.domain;

import org.springframework.data.mongodb.core.mapping.Document;

@Document
public class Province {
    private String name;
    private Integer aLatitude;
    private Integer aLongitude;
    private Integer bLatitude;
    private Integer bLongitude;

    private Province(String name, Integer aLatitude, Integer aLongitude,
                     Integer bLatitude, Integer bLongitude) {
        this.name = name;
        this.aLatitude = aLatitude;
        this.aLongitude = aLongitude;
        this.bLatitude = bLatitude;
        this.bLongitude = bLongitude;
    }

    public static Province build(String name, Integer aLatitude, Integer aLongitude,
                                 Integer bLatitude, Integer bLongitude) {
        if ((aLatitude < bLatitude) && (aLongitude > bLongitude)) {
            return new Province(name, aLatitude, aLongitude, bLatitude, bLongitude);
        }
        return null;
    }

    public String getName() {
        return name;
    }

    public Integer getALatitude() {
        return aLatitude;
    }

    public Integer getALongitude() {
        return aLongitude;
    }

    public Integer getBLatitude() {
        return bLatitude;
    }

    public Integer getBLongitude() {
        return bLongitude;
    }

    public boolean contains(Property property) {
        return (this.aLatitude <= property.getLat() && property.getLat() <= this.bLatitude) &&
                (this.bLongitude <= property.getLong() && property.getLong() <= this.aLongitude);
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("Name: ").append(getName());
        sb.append(", Upper left coordinate: [").append(getALatitude()).append(',').
                append(getALongitude()).append(']');
        sb.append(", Bottom right coordinate: [").append(getBLatitude()).append(',').
                append(getBLongitude()).append(']');
        return sb.toString();
    }
}
