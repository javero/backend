package br.com.vivareal.spotippos.services;

import br.com.vivareal.spotippos.domain.Province;
import br.com.vivareal.spotippos.domain.repositories.ProvinceRepository;
import org.springframework.stereotype.Service;

import javax.inject.Inject;

@Service
public class ProvincesService {
    @Inject
    private ProvinceRepository provinceRepository;

    public Province insert(Province province) {
        provinceRepository.insert(province);
        return province;
    }
}
