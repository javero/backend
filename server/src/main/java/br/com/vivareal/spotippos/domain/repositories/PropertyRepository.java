package br.com.vivareal.spotippos.domain.repositories;

import br.com.vivareal.spotippos.domain.Property;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface PropertyRepository extends MongoRepository<Property, Long> {

    Property findOne(Long id);

    Property insert(Property property);

    @Query("{'longitude': {$gte : ?3, $lte : ?1}, 'latitude': {$gte : ?0, $lte : ?2}}")
    List<Property> findAllInRegion(Integer ax, Integer ay, Integer bx, Integer by);
}
