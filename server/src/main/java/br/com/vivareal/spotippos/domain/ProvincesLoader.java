package br.com.vivareal.spotippos.domain;

import br.com.vivareal.spotippos.services.ProvincesService;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.ApplicationListener;
import org.springframework.core.env.Environment;
import org.springframework.data.mongodb.MongoDbFactory;
import org.springframework.stereotype.Component;

import javax.inject.Inject;
import java.io.IOException;
import java.io.InputStream;
import java.util.Iterator;
import java.util.Map;

@Component
public class ProvincesLoader implements ApplicationListener<ApplicationReadyEvent> {

    @Autowired
    Environment env;

    private final MongoDbFactory mongo;
    @Autowired
    public ProvincesLoader(MongoDbFactory mongo) {
        this.mongo = mongo;
    }

    @Inject
    ProvincesService provincesService;

    private static final Logger logger = LoggerFactory.getLogger(ProvincesLoader.class);

    @Override
    public void onApplicationEvent(final ApplicationReadyEvent event) {
        loadProvincesFromFile(env.getProperty("provinces.json.file"));
    }


    public void loadProvincesFromFile(String filename) {
        JsonNode jsonNode = getRootNodeFromFile(filename);
        if (jsonNode != null) {
            Iterator<Map.Entry<String, JsonNode>> iterator = jsonNode.fields();
            while (iterator.hasNext()) {
                Map.Entry<String, JsonNode> provinceEntry = iterator.next();
                addProvinceToRepository(provinceEntry.getKey(), provinceEntry.getValue());
            }
        }
    }

    private JsonNode getRootNodeFromFile(String filename) {
        InputStream inputStream = getClass().getResourceAsStream(filename);
        ObjectMapper mapper = new ObjectMapper();
        try {
            return mapper.readTree(inputStream);
        } catch (IOException e) {
            logger.error("Provinces file not found!", e);
        }
        return null;
    }

    private void addProvinceToRepository(String name, JsonNode jsonNode) {
        JsonNode upperLeft = jsonNode.findValue("upperLeft");
        JsonNode bottomRight = jsonNode.findValue("bottomRight");
        Province province = Province.build(name,
            upperLeft.findValue("x").intValue(), upperLeft.findValue("y").intValue(),
            bottomRight.findValue("x").intValue(), bottomRight.findValue("y").intValue());
        if (province != null) {
            logger.info("Inserting province: "+province);
            provincesService.insert(province);
        } else {
            logger.error("Invalid province: "+province);
        }
    }
}