# Candidato Julio Avero - Backender

## Tecnologias escolhidas

Para este desafio as principais ferramentas utilizadas foram:
* Linguagem/Plataforma **Java 8**;
* Framework **Spring** para "Inversão de controle" e "Injeção de dependência";
* Biblioteca **Jersey** como provedor da solução JAX-RS para serviços RESTFUL;
* Servidor **Apache Tomcat** embutido na aplicação;
* **MongoDB** para persistência de dados, com instância embutida na própria aplicação;
* Biblioteca **Jackson** para leitura/geração de dados JSON;
* Biblioteca **JUnit** para construção de testes;
* **Apache Maven** como ferramenta de automação de builds;
* Bibliotecas **Slf4j** e **Log4j** para geração e customização de logs da aplicação;
* **OneJar** para criação de um JAR unificado, para facilitar na distribuição da aplicação.

Para documentar a API e facilitar sua validação, foi utilizada a ferramenta [Swagger](http://swagger.io).

## Índice dos arquivos

### Arquivo */docs/JunitTests-Results.html*
Contém relatório em formato HTML, dos testes implementados para o projeto do Backend

### Diretório */docs/coverage-report*
Contém relatório sobre cobertura de testes. O ponto inicial de navegação é o index.html 

### Diretório */docs/code-challenge*
Cópia do [projeto](https://github.com/VivaReal/code-challenge/) com as specs do desafio.

### Diretório */docs/swagger-ui*
Contém uma cópia do Swagger-UI, feito em NodeJS, que pode ser instalado e iniciado através dos seguintes comandos:
* npm install
* npm run serve

Para os comandos acima é necessário a existência do pacote NPM no ambiente.

Alternativamente, a especificação da API pode ser visualizada pelo [editor online do Swagger](http://editor.swagger.io). A definição da API pode ser encontrada no arquivo **docs/swagger-ui/dist/rest-api-swagger.yml**.

### Diretório */server*
Nesta pasta encontra-se o projeto Java que implementa a API do desafio Spotippos!
Para a compilação e execução do serviço, é necessário a presença:
* do Java JDK 1.8;
* e do Apache Maven 3.3.9 ou superior;

Considerando que ambos utilitários estejam presentes em seu ambiente, basta agora executar, a partir do diretório /server:
* mvn clean install
* java -jar target/spotippos-0.0.1-SNAPSHOT.one-jar.jar

Assim que o processo de inicialização terminar, os serviços estarão disponívels para uso em http://localhost:8080/

A cada vez que o serviço for inicializado, o repositório de dados é limpo. Ou seja, tudo que foi persistido no ciclo de vida anterior será descartado. E a cada inicialização, o aplicativo reconfigura as definições das províncias de Spotippos a partir do arquivo **provinces.json** acessível via classpath.

### Diretório */tools*

Na raiz deste subdiretório tem um script feito em Ruby chamado ***load_properties.rb*** que foi utilizado durante o desenvolvimento para povoar o banco Mongo com propriedades contidas no arquivo **/docs/code-challenge/properties.json**

Como ele estava com a performance um pouco ruim, foi feito um outro utilitário em Java que faz a mesma tarefa, porém de forma multi-tarefa. Este utilitário abre 20 threads, o que melhorou e muito o tempo de importação para fins de teste. Este utilitário serviu também para medir performance, assim como o utilitário **siege**.

Qualquer dúvida, por favor entrem em contato!! ;D
<julio.avero at gmail.com>
